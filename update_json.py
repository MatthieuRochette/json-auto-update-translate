import json
import os
import collections.abc


def update_remove_non_existing_keys(dest: dict, source: dict) -> dict:
    dest_copy = dest.copy()
    for k, v in dest_copy.items():
        if isinstance(v, collections.abc.Mapping):
            try:
                dest[k] = update_remove_non_existing_keys(v, source[k])
            except KeyError:
                del dest[k]
        else:
            try:
                source[k]
            except KeyError:
                del dest[k]

    return dest


def update_add_non_existing_keys(dest: dict, source: dict) -> dict:
    for k, v in source.items():
        if isinstance(v, collections.abc.Mapping):
            dest[k] = update_add_non_existing_keys(dest.get(k, {}), v)
            dest[k] = update_remove_non_existing_keys(dest.get(k, {}), v)
        else:
            try:
                dest[k]
            except KeyError:
                dest[k] = v

    return dest


def main(origin_file: str):
    if not origin_file:
        print("Error: No origin file.")
        exit(0)

    files: list[str] = os.listdir(".")
    json_files: list[str] = []

    for file in files:
        if file.endswith(".json"):
            json_files.append(file)

    json_origin: dict

    try:
        json_files.remove(origin_file)
        with open(origin_file, "r", encoding="utf-8") as origin:
            json_origin = json.loads(origin.read())
        with open(origin_file, "w", encoding="utf-8") as origin:
            origin.write(json.dumps(json_origin, indent=4, sort_keys=True, ensure_ascii=False))
        with open(origin_file, "r", encoding="utf-8") as dest:
            print(origin_file + ": " + str(len(dest.readlines())) + " lines")
    except (ValueError, FileNotFoundError) as e:
        print(f"Error with file: {origin_file}")
        print(e)


    for file_name in json_files:
        json_dest: dict
        with open(file_name, "r", encoding="utf-8") as dest:
            json_dest = json.loads(dest.read())
            json_dest = update_add_non_existing_keys(json_dest, json_origin)
        with open(file_name, "w", encoding="utf-8") as dest:
            dest.write(json.dumps(json_dest, indent=4, sort_keys=True, ensure_ascii=False))
        with open(file_name, "r", encoding="utf-8") as dest:
            print(file_name + ": " + str(len(dest.readlines())) + " lines")


if __name__ == "__main__":
    from sys import argv

    origin_file=argv[1]
    main(origin_file)