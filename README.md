# JSON automatic update and translate

Do you too have an i18n from JSON file that is a hell to translate by hand? Fear no more, use my Python script to translate for free your files automatically using Deepl 🙂


## How does it work?

### Prerequisites
1. Python and pip installed, version 3.7+
2. Install the Deepl API connector for Python:
`pip install deepl`

### Usage
1. In the root of this repo, create your files with the language code as name (for example, `fr` for French and `en` for English ; it needs to be [accepted by the Deepl API](https://www.deepl.com/docs-api/translate-text/translate-text/) although no worry about the caps, i gotchu), and fill with your JSON data to translate the one corresponding to the language you already have. For instance, if you already have the i18n in French, fill in your `fr.json`.
2. Run the `sort_json_keys.py` script to put it all in alphabetical order (yes, it is very useful, especially when trying to read and compare your files):
```bash
python sort_json_keys.py
```
3. Update the keys of the other files with the `update_json.py` script with the origin file as a command line parameter (which will also automatically fill the new keys with the original language value, reading the wrong language is better than having nothing to read):
```bash
python update_json.py fr.json
```
4. Create an [API account on Deepl](https://www.deepl.com/pro-api?cta=menu-pro-api) (it's free within a certain number of translated characters) and add the API key in the `translate_json.py` file.
5. Run the automatic translation script with the origin file as a command parameter:
```bash
python translate_json.py fr.json
```

### Example of supported format:
Basically any dictionary-like JSON that contains only one language. Here is an example:

#### Extract from my own `fr.json` that led me to create this script
```json
{
    "self_profile": {
        "profile_parameters": {
            "edit_email": {
                "cancel_button": "Annuler",
                "edit_button": "Modifier",
                "email_label": "Nouvelle adresse email",
                "email_placeholder": "Votre nouvelle adresse email",
                "fetching": "Modification en cours...",
                "ko": "Nous n'avons pas réussi a modifier votre adresse email.",
                "ok": "Votre adresse email a bien été modifiée.",
                "title": "Modifier votre adresse email"
            },
            "edit_password": {
                "cancel_button": "Annuler",
                "confirm_new_password_label": "Confirmer le mot de passe",
                "confirm_new_password_placeholder": "Votre nouveau mot de passe",
                "edit_button": "Modifier",
                "fetching": "Modification en cours...",
                "ko": "Nous n'avons pas réussi a modifier votre mot de passe.",
                "new_password_label": "Nouveau mot de passe",
                "new_password_placeholder": "Votre nouveau mot de passe",
                "not_match": "Les mots de passe ne correspondent pas.",
                "ok": "Votre mot de passe a bien été modifié",
                "old_password_label": "Mot de passe actuel",
                "old_password_placeholder": "Votre mot de passe actuel",
                "title": "Modifier votre mot de passe"
            },
            "edit_password_button": "Modifier",
            "email": "Votre adresse email",
            "password": "Mot de passe :",
            "title": "Paramètres du profil"
        },
        "profile_picture": {
            "description": "L'image doit être au format JPEG, PNG ou GIF et ne doit pas dépasser 2 Mo.",
            "fetching": "Changement de photo de profil...",
            "ko": "Une erreur est survenue.",
            "modify_picture_button": "Mettre à jour votre photo de profil",
            "ok": "Votre photo de profil a bien été mise à jour.",
            "title": "Photo de profil",
            "too_large": "Votre photo doit faire maximum 2 Mo."
        }
    },
    "session_expired": "Votre session a expiré. Veuillez vous reconnecter."
}
```

## Fair warning
I have seen a bug that it doesn't always delete keys that are not present anyore in the origin file. BUT since it never deletes keys that shouldn't be, I never took the time to fix it. That's where the alphabetical order comes in handy, when comparing side-to-side for stray old keys.

## Note
You may complain that it is in three scripts to be run separately. And I understand you, but I'm a big slacker and it's easier to develop and test this way. So I'm putting a 4th script that simply calls the first 3 (don't forget the origin file as a command parameter), but if you want something optimized, then as the young ones say, DIY.
(tho if you do, a little Merge Request is welcome :-P)

## Credits
Of course, this README.md was translated with www.DeepL.com/Translator (free version)