from sys import argv

import sort_json_keys
import translate_json
import update_json

origin_file = argv[1]
sort_json_keys.main()
update_json.main(origin_file)
translate_json.main(origin_file)