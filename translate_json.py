import collections.abc
import json
import os

import deepl

auth_key = "YOUR_KEY_HERE"
translator = deepl.Translator(auth_key)

def translate_recursively(dest: dict, source: dict, source_lang: str, target_lang: str) -> dict:
    print('.', end="", flush=True)
    for k, v in source.items():
        if isinstance(v, collections.abc.Mapping):
            dest[k] = translate_recursively({}, v, target_lang)
        else:
            result = translator.translate_text(v, source_lang="", target_lang=target_lang)
            dest[k] = result.text
    return dest


def main(origin_file: str):
    if not origin_file:
        print("Error: No origin file.")
        exit(0)

    files: list[str] = os.listdir(".")
    json_files: list[str] = []

    for file in files:
        if file.endswith(".json"):
            json_files.append(file)

    json_files.remove(origin_file)
    with open(origin_file, "r", encoding="utf-8") as opened_origin:
        origin_json = json.loads(opened_origin.read())
    with open(origin_file, "w", encoding="utf-8") as opened_origin:
        origin_json = json.dumps(origin_json, indent=4, sort_keys=True, ensure_ascii=False)
    source_lang = origin_file.removesuffix(".json").upper()
    if source_lang == "EN":
            source_lang += "-GB"
    print("Source lang:", source_lang)
    for file in json_files:
        print(f"Translating {file}", flush=True)
        target_lang = file.removesuffix(".json").upper()
        if target_lang == "EN":
            target_lang += "-GB"
        print("Target lang:", target_lang)
        translated = translate_recursively({}, origin_json, source_lang, target_lang)
        with open(file, "w", encoding="utf-8") as opened_file:
            opened_file.write(json.dumps(translated, indent=4, sort_keys=True, ensure_ascii=False))
        print("", flush=True)


if __name__ == "__main__":
    from sys import argv

    origin_file=argv[1]
    main(origin_file)