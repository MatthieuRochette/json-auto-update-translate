import json
import os

def main():
    files: list[str] = os.listdir(".")
    json_files: list[str] = []

    for file in files:
        if file.endswith(".json"):
            json_files.append(file)

    json_origin: dict

    for file_name in json_files:
        json_dest: dict
        with open(file_name, "r", encoding="utf-8") as dest:
            json_dest = json.loads(dest.read())
        with open(file_name, "w", encoding="utf-8") as dest:
            dest.write(json.dumps(json_dest, indent=4, sort_keys=True, ensure_ascii=False))
        with open(file_name, "r", encoding="utf-8") as dest:
            print(file_name + ": " + str(len(dest.readlines())) + " lines")

if __name__ == "__main__":
    main()
